<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as SessionAdapter;

use Phalcon\Logger;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger\Adapter\File as FileLogger;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($config) {
	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);

	return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($config) {

	$view = new View();

	$view->setViewsDir($config->application->viewsDir);

	$view->registerEngines(array(
		'.phtml' => function ($view, $di) use ($config) {

			$volt = new VoltEngine($view, $di);

			$volt->setOptions(array(
				'compiledPath' => $config->application->cacheDir . 'templates/',
				'compiledExtension' => '.cache',
				'compiledSeparator' => '-'
			));

			return $volt;
		}
	));

	return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($config) {
	$dbConfig = $config->database->toArray();
	$adapter = $dbConfig['adapter'];
	unset($dbConfig['adapter']);


	$class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

	$connection = new $class($dbConfig);

	$eventsManager = new EventsManager();

	$logger = new FileLogger(APP_PATH . "/app/logs/debug.log");

	// Listen all the database events
	$eventsManager->attach('db', function ($event, $connection) use ($logger) {
		if ($event->getType() == 'beforeQuery') {
			$logger->log($connection->getSQLStatement(), Logger::INFO);
		}
	});
	$connection->setEventsManager($eventsManager);

	return $connection;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
	return new MetaData();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
	return new Flash(array(
		'error' => 'alert alert-danger',
		'success' => 'alert alert-success',
		'notice' => 'alert alert-info',
		'warning' => 'alert alert-warning'
	));
});
/**
 * 公共方法
 */
$di->set('functions', function () {
	require_once APP_PATH . 'app/library/FileUpload.php';
	return new FileUpload();
});

/**
 * 文件上传
 */
$di->set('functions', function () {
	require_once APP_PATH . 'app/library/functions.php';
	return new functions();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
	$session = new SessionAdapter();
	$session->start();

	return $session;
});
