<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

if ($_SERVER['SERVER_NAME'] == 'btest.mofox.cn') {
	$uploadUrl = 'http://statictest.mofox.cn';
	//图片路径
	$publicPath = str_replace('\\', '/', substr(__FILE__, 0, strpos(__FILE__, 'interlock')));
} elseif ($_SERVER['SERVER_NAME'] == 'b.mofox.cn') {
	$uploadUrl = 'http://static.mofox.cn';
	//图片路径
	$publicPath = str_replace('\\', '/', substr(__FILE__, 0, strpos(__FILE__, 'interlock')));
} elseif ($_SERVER['SERVER_NAME'] == 'bdev.mofox.cn') {
	$uploadUrl = 'http://staticdev.mofox.cn';
	//图片路径
	$publicPath = str_replace('\\', '/', substr(__FILE__, 0, strpos(__FILE__, 'interlock')));
}else{
	$uploadUrl = 'http://staticdev.mofox.cn';
	//图片路径
	$publicPath = str_replace('\\', '/', substr(__FILE__, 0, strpos(__FILE__, 'interlock')));
}

return new \Phalcon\Config(array(
	'database' => array(
		'adapter' => 'Mysql',
		'host' => '192.168.1.111',
		'username' => 'root',
		'password' => 'justd0!t',
		'dbname' => 'mofox',
		'charset' => 'utf8',
	),
	'application' => array(
		'controllersDir' => APP_PATH . '/app/controllers/',
		'modelsDir' => APP_PATH . '/app/models/',
		'migrationsDir' => APP_PATH . '/app/migrations/',
		'viewsDir' => APP_PATH . '/app/views/',
		'pluginsDir' => APP_PATH . '/app/plugins/',
		'libraryDir' => APP_PATH . '/app/library/',
		'cacheDir' => APP_PATH . '/app/cache/',
		'baseUri' => '/',
	),
	'paths' => array(
		'uploaded' => $uploadUrl . '/img/api/',
		'uploadTo' => $publicPath . 'static/img/api'
	)
));
