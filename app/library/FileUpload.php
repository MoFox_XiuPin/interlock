<?php
use Phalcon\Config\Adapter\Php as ConfigPhp;

class FileUpload
{

	private static $filetype = array('image/jpeg' => 'jpg',
		'image/jpg' => 'jpg',
		'image/jpeg' => 'jpg',
		'image/png' => 'png',
		'image/gif' => 'gif',
	);
	private static $fileext = array('jpg', 'png', 'gif', 'jpeg');

	/* 上传文件统一处理 */

	public static function upload($src, $filename, $action = 0)
	{
		global $appconfig;
		$datepath = date('Y-m-d');
		if (is_file($src) == false) {
			return false;
		}

		$ext = substr($filename, strrpos($filename, '.') + 1);

		if (!in_array($ext, self::$fileext)) {
			return false;
		}
		//处理成正确方向
		//self::CheckOrientation($src);
		$md5 = md5_file($src);
		$fname = $datepath . '_' . sprintf("%s.%s", $md5, $ext);
		$upload_path['upload_path'] = $appconfig['public_upload_path']; //使用用户端的url地址
		//如果不存在建立目录
		if (!file_exists($upload_path['upload_path'])) {
			mkdir($upload_path['upload_path']);
		}
		if (!file_exists($upload_path['upload_path'] . '/' . $datepath)) {
			mkdir($upload_path['upload_path'] . '/' . $datepath);
		}
		if (move_uploaded_file($src, $upload_path['upload_path'] . '/' . $datepath . '/' . $fname)) {
			//生成缩略图
			//用户头像
			if ($action == 1) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 182, 182); //我的头像
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 80, 80); //个人资料
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 60, 60); //评论
			}
			//商家logo
			if ($action == 2) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 60, 60); //订单
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 80, 80); //详情
			}

			//商品图片
			if ($action == 3) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 480, 640); //详情的
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 340, 290); //列表的
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 140, 140); //订单里面
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 80, 80); //查看详情的缩略图
			}


			//商品评论
			if ($action == 4) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 120, 120); //详情页面的
			}
			//关注博文
			if ($action == 5) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 170, 170); //列表
			}
			//商家背景
			if ($action == 6) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 600, 240); //商户列表
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 480, 640); //详情显示的商家图片
			}

			//风格图片
			if ($action == 7) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 250, 200); //逛街里显示
			}
			//商品分类图片
			if ($action == 8) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 170, 214); //逛街里显示
			}
			//banner图片
			if ($action == 9) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 410, 340); //banner图片
			}
			//买手头像
			if ($action == 10) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 52, 52); //潮流资讯
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 60, 60); //关注博文
			}
			//商品配送图
			if ($action == 11) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 100, 100); //上门试衣订单列表
			}
			//潮流资讯文章图片
			if ($action == 12) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 110, 110); //首页-潮流资讯图片
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 580, 400); //潮流资讯文章列表
			}
			//首页middle图
			if ($action == 13) {
				self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $fname, 290, 350); //首页-首页middle图
			}
			return $fname;
		}
		return false;
	}

	/* 获取url */

	public static function geturlbak($fname, $ismulti = false)
	{
		global $appconfig;
		$qbox['qbox_url'] = $appconfig['public_upload_url']; //使用用户端的url地址
		if ($ismulti == false) {
			if (substr(trim($fname), 0, 5) == 'http:' || substr(trim($fname), 0, 6) == 'https:') {
				return $fname;
			} else {
				return !empty($fname) ? $qbox['qbox_url'] . $fname : '';
			}
		} else {
			$imgs = explode(',', $fname);
			foreach ($imgs as $k1 => $z1) {
				$imgs[$k1] = self::geturl($z1);
			}
			return $imgs;
		}
	}

	/* 获取url
	 * $fname 文件名
	 * $ismulti 是否是数组
	 * $w 宽
	 * $h 高
	 */

	public static function geturl($fname, $ismulti = false, $w = 0, $h = 0)
	{
		$config = include APP_PATH . "/app/config/config.php";
		$pathConfig = $config->paths;
		$qbox['qbox_url'] = $pathConfig->uploaded; //使用用户端的url地址
		if ($ismulti == false) {
			$datepath = '';
			//判断是否有http开头地址
			if ($pos = strpos($fname, '_')) {
				if (strrpos($fname, '/') > 0) {
					$datepath = substr($fname, strrpos($fname, '/') + 1, $pos - strrpos($fname, '/') - 1);
				} else {
					$datepath = substr($fname, 0, $pos);
				}
			}
			//如果有尺寸查找缩略图
			if ($w > 0 && $h > 0) {
				if (empty($fname)) {
					return '';
				}
				if (strrpos($fname, '/') > 0) {
					$oldfname = substr($fname, strrpos($fname, '/') + 1);
					$oldfname = str_replace('undefined', '', $oldfname);
				} else {
					$oldfname = $fname;
				}

				$fname = substr($fname, 0, strrpos($fname, '.')) . '_' . $w . 'V' . $h . '.' . substr($fname, strrpos($fname, '.') + 1);
				$upload_path['upload_path'] = $appconfig['public_upload_path']; //使用用户端的上传路径
				if (strlen($datepath) > 0) {
					//  exit();
					//源文件存在
					if (file_exists($upload_path['upload_path'] . '/' . $datepath . '/' . $oldfname)) {
						if (file_exists($upload_path['upload_path'] . '/' . $datepath . '/' . $fname) == false) {
							self::drawImg($upload_path['upload_path'] . '/' . $datepath . '/' . $oldfname, $w, $h); //生成缩略图
						}
					}
				} else {
					//源文件存在(兼容旧版)
					if (file_exists($upload_path['upload_path'] . '/' . $oldfname)) {
						if (file_exists($upload_path['upload_path'] . '/' . $fname) == false) {
							self::drawImg($upload_path['upload_path'] . '/' . $oldfname, $w, $h); //生成缩略图
						}
					}
				}
			}

			if (substr(trim($fname), 0, 5) == 'http:' || substr(trim($fname), 0, 6) == 'https:') {
				return $fname;
			} else {
				if (strlen($datepath) > 0) {
					return !empty($fname) ? $qbox['qbox_url'] . $datepath . '/' . $fname : '';
				}
				return !empty($fname) ? $qbox['qbox_url'] . $fname : '';
			}
		} else {
			$imgs = explode(',', $fname);
			foreach ($imgs as $k1 => $z1) {
				$imgs[$k1] = self::geturl($z1);
			}
			return $imgs;
		}
	}

	/* 获取url列表 */

	public static function geturlall(&$list, $key)
	{
		$list = self::object2array($list);
		foreach ($list as $k => $z) {
			if ($z[$key]) {
				$imgs = explode(',', $z[$key]);
				foreach ($imgs as $k1 => $z1) {
					$imgs[$k1] = self::geturl($z1);
				}
				$list[$k][$key] = $imgs;
			} else {
				$list[$k][$key] = array();
			}
		}
		//  return $list;
	}

	/* 对象转数组 */

	public static function object2array(&$object)
	{
		$object = json_decode(json_encode($object), true);
		return $object;
	}

	public static function geturlallthumb(&$list, $key, $w = 0, $h = 0)
	{
		$list = self::object2array($list);
		foreach ($list as $k => $z) {
			if ($z[$key]) {
				$imgs = explode(',', $z[$key]);
				foreach ($imgs as $k1 => $z1) {
					unset($imgs[$k1]);
					$imgs[$k1]['thumb'] = self::geturl($z1, false, $w, $h);
					$info = self::getimgsize($imgs[$k1]['thumb']);
					$imgs[$k1]['src'] = self::geturl($z1);
					$imgs[$k1]['width'] = (int)$info[0];
					$imgs[$k1]['height'] = (int)$info[1];
				}
				$list[$k][$key] = $imgs;
			} else {
				$list[$k][$key] = array();
			}
		}
		//  return $list;
	}

	public static function getimgsize($fname)
	{
		global $appconfig;
		//
		//截取文件名
		$fname = substr($fname, strrpos($fname, '/'));
		//根据日期获取图片
		if ($pos = strpos($fname, '_')) {
			$datepath = substr($fname, 0, $pos);
		}
		$upload_path = $appconfig['public_upload_path']; //使用用户端的url地址
		//  echo $upload_path .'/'. (strlen($datepath) > 0 ? $datepath : '/') . $fname.'<br/>';
		return @getimagesize($upload_path . (strlen($datepath) > 0 ? $datepath : '/') . $fname);
	}

	/* 等比例缩放(用于生成缩略图) */

	public static function drawImg($from, $w = 100, $h = 100)
	{
		$info = getimagesize($from);
		switch ($info[2]) {
			case 1:
				$im = imagecreatefromgif($from);
				break;
			case 2:
				$im = imagecreatefromjpeg($from);
				break;
			case 3:
				$im = imagecreatefrompng($from);
				break;
			default:
				exit('不支持的图像格式');
				break;
		}
		$temp = pathinfo($from);
		$name = $temp["basename"]; //文件名
		$dir = $temp["dirname"]; //文件所在的文件夹
		$extension = $temp["extension"]; //文件扩展名
		$width = $info[0]; //获取图片宽度
		$height = $info[1]; //获取图片高度
		$per1 = round($width / $height, 2); //计算原图长宽比
		$per2 = round($w / $h, 2); //计算缩略图长宽比
		//计算缩放比例
		if ($per1 > $per2 || $per1 == $per2) {
			//原图长宽比大于或者等于缩略图长宽比，则按照宽度优先
			$per = $w / $width;
		}
		if ($per1 < $per2) {
			//原图长宽比小于缩略图长宽比，则按照高度优先
			$per = $h / $height;
		}
		$temp_w = intval($width * $per) * 1.68; //计算原图缩放后的宽度   如果是1080的。。设计图是640，乘于1.68
		$temp_h = intval($height * $per) * 1.68; //计算原图缩放后的高度   如果是1080的。。设计图是640，乘于1.68
		$dst_im = imagecreatetruecolor($temp_w, $temp_h);
		//调整大小
		imagecopyresized($dst_im, $im, 0, 0, 0, 0, $temp_w, $temp_h, $width, $height);
		$newfile = substr($name, 0, strrpos($name, '.')) . '_' . $w . 'V' . $h . '.' . $extension;
		//输出缩小后的图像
		//exit($newfile);
		imagejpeg($dst_im, $dir . '/' . $newfile);
		imagedestroy($dst_im);
		imagedestroy($im);
		return 1;
	}

	public static function removehttp(&$arr)
	{
		if (!is_array($arr)) {
			return false;
		}
		foreach ($arr as $k => $z) {
			$arr[$k] = substr($z, strrpos($z, '/') + 1);
		}
		return true;
	}

	/* 获取url
	 * $fname 文件名
	 * $ismulti 是否是数组
	 * $w 宽
	 * $h 高
	 */

	public static function geturlthumb($fname, $w = 0, $h = 0)
	{
		if (strlen(trim($fname)) > 0) {
			$imgs = explode(',', $fname);
			foreach ($imgs as $k1 => $z1) {
				unset($imgs[$k1]);
				$imgs[$k1]['src'] = self::geturl($z1);
				$imgs[$k1]['thumb'] = self::geturl($z1, false, $w, $h);
				$info = self::getimgsize($imgs[$k1]['thumb']);
				$imgs[$k1]['width'] = (int)$info[0];
				$imgs[$k1]['height'] = (int)$info[1];
			}
			return $imgs;
		}
		return array();
	}

	/*处理成正确的方向*/
	public static function CheckOrientation($filename)
	{
		if (function_exists('exif_read_data')) {
			$image = imagecreatefromstring(file_get_contents($filename));
			$exif = exif_read_data($filename);
			if (!empty($exif['Orientation'])) {
				if ($exif['Orientation'] == 8 || $exif['Orientation'] == 6 || $exif['Orientation'] == 3) {
					switch ($exif['Orientation']) {
						case 8:
							$image = imagerotate($image, 90, 0);
							break;
						case 3:
							$image = imagerotate($image, 180, 0);
							break;
						case 6:
							$image = imagerotate($image, -90, 0);
							break;
					}
					imagejpeg($image, $filename);
					imagedestroy($image);
				}
			}

		}
	}

}
