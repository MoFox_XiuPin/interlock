<?php
use Phalcon\Http\Response;

/**
 * Created by PhpStorm.
 * User: Leon
 * Date: 2016/5/25
 * Time: 10:32
 */
class functions
{
	public static function send404()
	{
		$response = new Response();
		$response->setStatusCode(404, "Not Found");
		//$response->setContent("Sorry, the page doesn't exist");
		$response->send();
	}

	public static function isEmpty($input)
	{
		if (empty($input)) {
			return true;
		} else if (is_array($input)) {
			foreach ($input as $val) {
				if (empty($val)) return true;
			}
		}

		return false;
	}
}