<?php

class TestController extends \Phalcon\Mvc\Controller
{
	public function initialize()
	{
		$this->auth = $this->session->get('auth');
		if (!$this->auth) {
			$this->dispatcher->forward(
				array(
					'controller' => 'auth',
					'action' => 'login'
				)
			);
		}
	}
	public function indexAction()
	{

	}

}

