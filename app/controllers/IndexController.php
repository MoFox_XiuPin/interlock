<?php

use Phalcon\Http\Request;
use Phalcon\Logger;


class IndexController extends ControllerBase
{
	private $auth;

	private $field = ['province_id', 'city_id', 'area_id', 'id'];

	public function initialize()
	{
		$this->auth = $this->session->get('auth');
		if (!$this->auth) {
			$this->dispatcher->forward(
				array(
					'controller' => 'auth',
					'action' => 'login'
				)
			);
		}
	}

	public function indexAction()
	{
		if (!$this->auth) return;

		$end = strtotime('today');
		$start = strtotime("-29 days", $end);


		$data = [];

		$data['trend'] = $this->getTrendData($start, $end, 0, 1);
		$data['ingredient'] = $this->getIngredientData($start, $end, 0, 1);
		$data['rank'] = $this->getRankData($start, $end, 0, 1);
		$data['date'] = [$start, $end];
		//省份列表
		$province = Region::find(array('columns' => array('id', 'region_name as text'), 'conditions' => "region_type = 1"))->toArray();
		$this->view->province = json_encode($province);
		//
		$this->view->data = json_encode($data);
		//个人信息
		$this->view->auth = $this->auth;

	}

	public function testAction()
	{
		$end = strtotime('today');
		$start = strtotime("-29 days", $end);
		$this->getRankData($start, $end, 6, 1);
	}

	public function refreshDataAction()
	{
		$input = $this->checkInputData(['start', 'end', 'id', 'type']);
		if ($input) {
			$data['status'] = 1;
			$data['msg'] = '';

			$start = $input['start'];
			$end = $input['end'];
			$id = $input['id'];
			$type = $input['type'];


			$data['trend'] = $this->getTrendData($start, $end, $id, $type);;
			$data['ingredient'] = $this->getIngredientData($start, $end, $id, $type);
			$data['rank'] = $this->getRankData($start, $end, $id, $type);
		} else {
			$data['status'] = -1;
			$data['msg'] = "参数不正确";
		}

		exit(json_encode($data));
	}

	private function checkInputData($args)
	{
		$request = new Request();
		$tmp = [];
		if ($request->isAjax()) {
			foreach ($args as $arg) {
				$tmp[$arg] = trim($request->getPost($arg));
				if (empty($tmp[$arg]) && $tmp[$arg] === 0) return false;
			}
		} else {
			functions::send404();
			return false;
		}

		return $tmp;
	}

	public function getAreaDataAction()
	{
		$input = $this->checkInputData(['id']);
		if ($input) {
			$data['status'] = 1;
			$data['msg'] = '';
			$area = Region::find(array('columns' => array('id', 'region_name as text', 'region_type as type'),
				'conditions' => "parent_id = " . $input['id']))->toArray();
			$type = 2;
			if ($area) {
				$type = $area[0]['type'] - 2;
			}
			$shops = Shops::find(array('columns' => array('id', 'shop_name as text'),
				'conditions' => 'deleted_at = 0 AND ' . $this->field[$type] . ' = ' . $input['id'] . ' AND id IN(' . implode(', ', $this->auth['shopsId']) . ')'))->toArray();

			if ($shops) {
				array_unshift($shops, ['id' => 0, 'text' => '全部']);
				array_unshift($area, ['id' => 0, 'text' => '全部']);
				$data['shops'] = $shops;
			} else {
				$data['shops'] = [];
			}
			$data['data'] = $area;


		} else {
			$data['status'] = -1;
			$data['msg'] = "参数不正确";
		}
		exit(json_encode($data));
	}

	private function getShopsData($searchId, $searchType)
	{
		$conditions = 'deleted_at = 0';
		if ($searchId > 0) {
			$conditions .= ' AND ' . $this->field[$searchType] . ' = ' . $searchId;
		}

		$conditions .= ' AND id IN(' . implode(', ', $this->auth['shopsId']) . ')';
		$shops = Shops::find(array('columns' => 'id, shop_name', 'conditions' => $conditions))->toArray();

		$shopInfo = ['id' => null, 'name' => null];
		foreach ($shops as $shop) {
			$shopInfo['id'][] = $shop['id'];
			$shopInfo['name'][$shop['id']] = $shop['shop_name'];
		}

		return $shopInfo;
	}

	private function getRankData($start, $end, $searchId, $searchType)
	{
		$tmp = [];
		$shopInfo = $this->getShopsData($searchId, $searchType);
		if ($shopInfo['id']) {
			$data = [];
			$orders = Orders::find('deleted_at = 0 AND updated_at <= ' . $end . ' AND updated_at >= ' . $start . ' AND shop_id IN(' . implode(', ', $shopInfo['id']) . ')');
			foreach ($orders as $order) {
				if (($order->order_type == 3 && ($order->order_state == 6 || $order->order_state == 11)) ||
					($order->order_type == 2 && ($order->order_state == 3 || $order->order_state == 4)) ||
					($order->order_type == 1 && ($order->order_state == 4 || $order->order_state == 5))
				) {
					@$data[$order->shop_id]['count'] += 1;
					@$data[$order->shop_id]['money'] += $order->total_money;
					@$data[$order->shop_id]['sid'] = $order->shop_id;
				}
			}
			if ($data) {
				usort($data, [$this, "byMoney"]);
				foreach ($data as $key => $val) {
					$tmp['money']['name'][] = $shopInfo['name'][$val['sid']];
					$tmp['money']['money'][] = $val['money'];
				}
				usort($data, [$this, "byCount"]);
				foreach ($data as $key => $val) {
					$tmp['count']['name'][] = $shopInfo['name'][$val['sid']];
					$tmp['count']['money'][] = $val['count'];
				}
			}
		}
		//$tmp['name'] = ['测试1', '测试2', '测试3', '测试4', '测试5', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1', '测试1'];
		//$tmp['money'] = [123, 456, 123, 345, 234, 345, 456, 567, 678, 789, 890, 218, 456, 123, 345, 234, 345, 456, 567, 678, 789, 890, 218];
		return $tmp;
	}

	private function byCount($a, $b)
	{
		return $a["count"] - $b["count"];
	}

	private function byMoney($a, $b)
	{
		return $a["money"] - $b["money"];
	}

	private function getIngredientData($start, $end, $searchId, $searchType)
	{
		$data = [];
		$shopInfo = $this->getShopsData($searchId, $searchType);
		if ($shopInfo['id']) {
			$orders = Orders::find('deleted_at = 0 AND updated_at <= ' . $end . ' AND updated_at >= ' . $start . ' AND shop_id IN(' . implode(', ', $shopInfo['id']) . ')');
			foreach ($orders as $order) {
				if (($order->order_type == 3 && ($order->order_state == 6 || $order->order_state == 11)) ||
					($order->order_type == 2 && ($order->order_state == 3 || $order->order_state == 4)) ||
					($order->order_type == 1 && ($order->order_state == 4 || $order->order_state == 5))
				) {
					@$data['count'][$order->order_type - 1] += 1;
					@$data['countTotal'] += 1;
					@$data['money'][$order->order_type - 1] += $order->total_money;
					@$data['moneyTotal'] += $order->total_money;
				}
			}
		}
		return $data;
	}

	private function getTrendData($start, $end, $searchId, $searchType)
	{
		//order_state
		//order_type=1快递订单：1待付款，2待发货，3待收货，4已收货，5已评价，6申请退款中，7已退款，8已取消，9已超时，10处理中
		//order_type=2到店自取：1待付款，2已预订，3已收货，4交易成功（已评价），5已超时，6退款中，7已退款，8已取消
		//order_type=3上门试衣：1待接单，2取货中，3配送中，4试衣中，5不适合，6已收货（交易成功），7已超时，8交易关闭（不合适  ），9退款中（退配  送费），10已退款，11已评论，12已取消，13,已返还，14已发货，15待付款

		$data = [];
		$shopInfo = $this->getShopsData($searchId, $searchType);
		if ($shopInfo['id']) {
			$orders = Orders::find('deleted_at = 0 AND updated_at <= ' . $end . ' AND updated_at >= ' . $start . ' AND shop_id IN(' . implode(', ', $shopInfo['id']) . ')');
			foreach ($orders as $order) {
				if ($order->order_type == 3 && ($order->order_state == 6 || $order->order_state == 11)) {
					@$data[date('Y-m-d', $order->updated_at)] += $order->total_money;
				}

				if ($order->order_type == 2 && ($order->order_state == 3 || $order->order_state == 4)) {
					@$data[date('Y-m-d', $order->updated_at)] += $order->total_money;
				}

				if ($order->order_type == 1 && ($order->order_state == 4 || $order->order_state == 5)) {
					@$data[date('Y-m-d', $order->updated_at)] += $order->total_money;
				}
			}
		}
		$tmp = [];
		foreach ($data as $key => $val) {
			$tmp[] = ['day' => $key, 'value' => $val];
		}
		return $tmp;
	}

	private function convertAsterisk($str)
	{
		if (strlen($str) < 9) return $str;
		$asterisk = str_repeat('*', strlen($str) - 8);

		return substr($str, 0, 4) . $asterisk . substr($str, -4);
	}

	public function shopListAction()
	{
		if (!$this->auth) return;

		$shops = Shops::find('deleted_at = 0 AND id IN(' . implode(', ', $this->auth['shopsId']) . ')')->toArray();
		$partner_state = ['', '收录', '发动态', '合作'];
		foreach ($shops as &$shop) {
			$shop['goods_Num'] = GoodsInventory::findFirst(array('columns' => 'count(inventory_num) AS num', 'conditions' => 'deleted_at = 0 AND shop_id = ' . $shop['id']))->num;
			$orders = Orders::find('deleted_at = 0 AND shop_id = ' . $shop['id']);
			$sell_num = 0;
			$total_money = 0;
			foreach ($orders as $order) {
				if (($order->order_type == 3 && ($order->order_state == 6 || $order->order_state == 11)) ||
					($order->order_type == 2 && ($order->order_state == 3 || $order->order_state == 4)) ||
					($order->order_type == 1 && ($order->order_state == 4 || $order->order_state == 5))
				) {
					$sell_num += $order->buyer_num;
					$total_money += $order->total_money;
				}
			}

			$shop['sell_num'] = $sell_num;
			$shop['total_money'] = $total_money;
			$staffNum = ShopsStaff::findFirst(array('columns' => 'COUNT(id) AS num', 'conditions' => 'deleted_at = 0 AND shop_id = ' . $shop['id']))->num;
			$shop['staff_num'] = $staffNum;
			$shop['partner_state'] = $partner_state[$shop['partner_state']];
			$shop['shop_logo'] = FileUpload::geturl($shop['shop_logo']);
			$shop['shop_back'] = FileUpload::geturl($shop['shop_back']);
			$shop['open_time'] = date('H:i', $shop['start_time']) . '-' . date('H:i', $shop['end_time']);
			//登录信息
			$loginInfo = ShopsStaff::findFirst('deleted_at = 0 AND super_admin = 1 AND shop_id = ' . $shop['id']);
			$shop['login_name'] = $loginInfo->staff_name;
			$shop['login_mobile'] = $loginInfo->staff_mobile;

			//银行卡信息
			$cardInfo = ShopBankCard::findFirst('deleted_at = 0 AND shop_id = ' . $shop['id']);
			$shop['bank_name'] = $cardInfo->bank_name;
			$shop['branch_bank'] = $cardInfo->branch_bank;
			$shop['bank_card'] = $this->convertAsterisk($cardInfo->bank_card);
			$shop['identity_no'] = $this->convertAsterisk($cardInfo->identity_no);
			$shop['real_name'] = $cardInfo->real_name;

		}

		//$this->view->debug = $shops;
		$this->view->data = $shops;
		//省份列表
		$province = Region::find(array('columns' => array('id', 'region_name as text'), 'conditions' => "region_type = 1"))->toArray();
		$this->view->province = json_encode($province);
		//个人信息
		$this->view->auth = $this->auth;
	}
}

