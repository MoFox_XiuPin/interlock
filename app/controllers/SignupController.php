<?php

class SignupController extends \Phalcon\Mvc\Controller
{

	public function indexAction()
	{
		$this->tag->setTitle("test");
	}

	public function registerAction()
	{
		$user = new Users();

		$success = $user->save($this->request->getPost(), array("name", "email"));

		if ($success) {
			echo "Thanks for registering!";
		} else {
			echo "Sorry, the following problems were generated: ";
			foreach ($user->getMessages() as $message) {
				echo $message->getMessage(), "<br/>";
			}
		}
	}
}

