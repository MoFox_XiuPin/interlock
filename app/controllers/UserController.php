<?php

class UserController extends \Phalcon\Mvc\Controller
{
	public function initialize()
	{
		$auth = $this->session->get('auth');
		if (!$auth) {
			$dispatcher = $this->dispatcher;
			$this->session->set('dispatch', '/' . $this->router->getControllerName() . '/' . $this->router->getActionName() . '?' . implode('&', $dispatcher->getParams()));
			$this->dispatcher->forward(
				array(
					'controller' => 'auth',
					'action' => 'login'
				)
			);
		}
	}

	public function indexAction()
	{
		echo $this->router->getControllerName();
		echo $this->router->getActionName();
		$dispatcher = $this->dispatcher;
		echo $dispatcher->getControllerName();
		echo $dispatcher->getActionName();
	}

	public function loginAction()
	{

	}

}

