<?php
use Phalcon\Http\Request;
use Phalcon\Http\Response;

class AuthController extends \Phalcon\Mvc\Controller
{

	public function indexAction()
	{
	}

	public function doLoginAction()
	{

		$request = new Request();
		$data['status'] = 0;
		$data['msg'] = "成功";
		if ($request->isAjax()) {
			$account = strtolower(trim($request->get('user')));
			$password = trim($request->get('password'));
			if(empty($account) || empty($password)){
				$data['status'] = -1;
				$data['msg'] = "参数不正确";
				exit(json_encode($data));
			}

			$company = Company::findFirst("disabled = '0' AND account = '" . $account .'\'');
			if (empty($company)) {
				$data['status'] = 1;
				$data['msg'] = "用户不存在";
			} else {
				if ($company->password != md5($password)) {
					$data['status'] = 2;
					$data['msg'] = "用户名或密码错误";
				} else {
					$shops = $company->CompanyShops->toArray();
					$uAvatar = '/images/fox.png';
					$shopsId = '';
					if(count($shops) > 0){
						foreach ($shops as $shop){
							$avatar = Shops::findFirst(array('columns' => array('shop_logo'), 'conditions' => "deleted_at = 0 AND length(shop_logo) = 47 AND id = ".$shop['shop_id']));
							$shopsId[] = $shop['shop_id'];
							if($uAvatar == '/images/fox.png' && !empty($avatar)){
								$uAvatar = 	FileUpload::geturl($avatar->shop_logo);
							}
						}
					}
					$this->session->set('auth',
						array(
							'cid' => $company->id,
							'cName' => $company->name,
							'cAccount' => $company->account,
							'cLogo' => $uAvatar,
							'shopsId' => $shopsId,
							//'sid' => $company->Shops->id,
							//'sName' => $company->Shops->shop_name,
							'cAddress' => $company->address,
							//'sLogo' => FileUpload::geturl($company->Shops->shop_logo)
						));
				}
			}

			exit(json_encode($data));
		} else {
			//echo json_encode($data);
			functions::send404();
		}
	}

	public function loginAction()
	{

	}

	public function registerAction()
	{
		echo "registerAction";
	}

	public function logoutAction()
	{
		$this->session->destroy('auth');
		$this->dispatcher->forward(
			array(
				'controller' => 'auth',
				'action' => 'login'
			)
		);
	}

}

