<?php

class Region extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $parent_id;

	/**
	 *
	 * @var string
	 */
	public $region_name;

	/**
	 *
	 * @var integer
	 */
	public $region_type;

	/**
	 *
	 * @var integer
	 */
	public $status;

	/**
	 *
	 * @var integer
	 */
	public $created_at;

	/**
	 *
	 * @var integer
	 */
	public $updated_at;

	/**
	 *
	 * @var integer
	 */
	public $deleted_at;

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'region';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Region[]
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Region
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

}
