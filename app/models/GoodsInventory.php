<?php

class GoodsInventory extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $goods_id;

	/**
	 *
	 * @var integer
	 */
	public $goods_check_id;

	/**
	 *
	 * @var integer
	 */
	public $shop_id;

	/**
	 *
	 * @var string
	 */
	public $goods_color;

	/**
	 *
	 * @var string
	 */
	public $goods_size;

	/**
	 *
	 * @var integer
	 */
	public $inventory_num;

	/**
	 *
	 * @var string
	 */
	public $goods_avatar;

	/**
	 *
	 * @var double
	 */
	public $goods_price;

	/**
	 *
	 * @var double
	 */
	public $store_price;

	/**
	 *
	 * @var string
	 */
	public $goods_number;

	/**
	 *
	 * @var string
	 */
	public $weighs;

	/**
	 *
	 * @var integer
	 */
	public $created_at;

	/**
	 *
	 * @var integer
	 */
	public $updated_at;

	/**
	 *
	 * @var integer
	 */
	public $deleted_at;

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'goods_inventory';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return GoodsInventory[]
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return GoodsInventory
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

	public function initialize()
	{
		$this->belongsTo('shop_id', 'Shops', 'id');
	}

}
