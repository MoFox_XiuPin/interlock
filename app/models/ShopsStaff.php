<?php

class ShopsStaff extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var string
	 */
	public $shop_id;

	/**
	 *
	 * @var integer
	 */
	public $province_id;

	/**
	 *
	 * @var integer
	 */
	public $city_id;

	/**
	 *
	 * @var integer
	 */
	public $create_people;

	/**
	 *
	 * @var integer
	 */
	public $super_admin;

	/**
	 *
	 * @var string
	 */
	public $staff_name;

	/**
	 *
	 * @var string
	 */
	public $staff_mobile;

	/**
	 *
	 * @var integer
	 */
	public $code;

	/**
	 *
	 * @var string
	 */
	public $password;

	/**
	 *
	 * @var string
	 */
	public $authority;

	/**
	 *
	 * @var string
	 */
	public $staff_avatar;

	/**
	 *
	 * @var integer
	 */
	public $created_at;

	/**
	 *
	 * @var integer
	 */
	public $updated_at;

	/**
	 *
	 * @var integer
	 */
	public $deleted_at;

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'shops_staff';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return ShopsStaff[]
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return ShopsStaff
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

	public function initialize()
	{
		$this->belongsTo('shop_id', 'Shops', 'id');
	}
}
