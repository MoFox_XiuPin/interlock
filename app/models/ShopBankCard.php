<?php

class ShopBankCard extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $shop_id;

	/**
	 *
	 * @var string
	 */
	public $mobile;

	/**
	 *
	 * @var string
	 */
	public $identity_no;

	/**
	 *
	 * @var string
	 */
	public $real_name;

	/**
	 *
	 * @var integer
	 */
	public $bank_id;

	/**
	 *
	 * @var string
	 */
	public $bank_name;

	/**
	 *
	 * @var string
	 */
	public $bank_card;

	/**
	 *
	 * @var integer
	 */
	public $code;

	/**
	 *
	 * @var string
	 */
	public $password;

	/**
	 *
	 * @var integer
	 */
	public $first_pwd_status;

	/**
	 *
	 * @var integer
	 */
	public $check_state;

	/**
	 *
	 * @var string
	 */
	public $branch_bank;

	/**
	 *
	 * @var integer
	 */
	public $credentials_type;

	/**
	 *
	 * @var integer
	 */
	public $created_at;

	/**
	 *
	 * @var integer
	 */
	public $updated_at;

	/**
	 *
	 * @var integer
	 */
	public $deleted_at;

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'shop_bank_card';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return ShopBankCard[]
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return ShopBankCard
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

}
