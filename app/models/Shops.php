<?php

class Shops extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var string
	 */
	public $shop_name;

	/**
	 *
	 * @var string
	 */
	public $brand_name;

	/**
	 *
	 * @var string
	 */
	public $shop_intro;

	/**
	 *
	 * @var string
	 */
	public $shop_tel;

	/**
	 *
	 * @var string
	 */
	public $staff_mobile;

	/**
	 *
	 * @var string
	 */
	public $address;

	/**
	 *
	 * @var string
	 */
	public $shop_logo;

	/**
	 *
	 * @var string
	 */
	public $shop_back;

	/**
	 *
	 * @var double
	 */
	public $shipping_fee;

	/**
	 *
	 * @var integer
	 */
	public $province_id;

	/**
	 *
	 * @var integer
	 */
	public $city_id;

	/**
	 *
	 * @var integer
	 */
	public $region_id;

	/**
	 *
	 * @var integer
	 */
	public $area_id;

	/**
	 *
	 * @var integer
	 */
	public $sales_count;

	/**
	 *
	 * @var integer
	 */
	public $attention_num;

	/**
	 *
	 * @var integer
	 */
	public $parent_shop_id;

	/**
	 *
	 * @var string
	 */
	public $shop_type;

	/**
	 *
	 * @var integer
	 */
	public $start_time;

	/**
	 *
	 * @var integer
	 */
	public $close_time;

	/**
	 *
	 * @var integer
	 */
	public $sub_shop_state;

	/**
	 *
	 * @var string
	 */
	public $sign_file;

	/**
	 *
	 * @var integer
	 */
	public $sign_time;

	/**
	 *
	 * @var integer
	 */
	public $online_time;

	/**
	 *
	 * @var integer
	 */
	public $online_state;

	/**
	 *
	 * @var integer
	 */
	public $recommend_state;

	/**
	 *
	 * @var integer
	 */
	public $shop_grade;

	/**
	 *
	 * @var integer
	 */
	public $support_buy;

	/**
	 *
	 * @var integer
	 */
	public $partner_state;

	/**
	 *
	 * @var string
	 */
	public $shop_img;

	/**
	 *
	 * @var string
	 */
	public $longitude;

	/**
	 *
	 * @var string
	 */
	public $latitude;

	/**
	 *
	 * @var integer
	 */
	public $try_brokerage;

	/**
	 *
	 * @var integer
	 */
	public $brend_brokerage;

	/**
	 *
	 * @var integer
	 */
	public $new_goods_brokerage;

	/**
	 *
	 * @var string
	 */
	public $store_photos;

	/**
	 *
	 * @var integer
	 */
	public $created_at;

	/**
	 *
	 * @var integer
	 */
	public $updated_at;

	/**
	 *
	 * @var integer
	 */
	public $deleted_at;

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'shops';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Shops[]
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Shops
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

	public function initialize()
	{
		$this->hasMany("id", "ShopsStaff", "shop_id");
	}
}
