<?php

class Orders extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var string
	 */
	public $order_no;

	/**
	 *
	 * @var integer
	 */
	public $user_id;

	/**
	 *
	 * @var integer
	 */
	public $shop_id;

	/**
	 *
	 * @var integer
	 */
	public $shipping_id;

	/**
	 *
	 * @var string
	 */
	public $buyer_mobile;

	/**
	 *
	 * @var double
	 */
	public $total_money;

	/**
	 *
	 * @var double
	 */
	public $post_charge;

	/**
	 *
	 * @var integer
	 */
	public $order_state;

	/**
	 *
	 * @var integer
	 */
	public $buyer_num;

	/**
	 *
	 * @var integer
	 */
	public $order_type;

	/**
	 *
	 * @var integer
	 */
	public $address_id;

	/**
	 *
	 * @var integer
	 */
	public $try_time;

	/**
	 *
	 * @var integer
	 */
	public $send_type;

	/**
	 *
	 * @var integer
	 */
	public $try_long_time;

	/**
	 *
	 * @var double
	 */
	public $try_money;

	/**
	 *
	 * @var integer
	 */
	public $unreason;

	/**
	 *
	 * @var integer
	 */
	public $shipping_state;

	/**
	 *
	 * @var string
	 */
	public $remark;

	/**
	 *
	 * @var string
	 */
	public $shipping_img;

	/**
	 *
	 * @var string
	 */
	public $goods_img;

	/**
	 *
	 * @var string
	 */
	public $cancel_reason;

	/**
	 *
	 * @var integer
	 */
	public $coupon_id;

	/**
	 *
	 * @var integer
	 */
	public $try_coupon_id;

	/**
	 *
	 * @var string
	 */
	public $order_pay_no;

	/**
	 *
	 * @var string
	 */
	public $shipment_pay_no;

	/**
	 *
	 * @var double
	 */
	public $order_amount;

	/**
	 *
	 * @var integer
	 */
	public $remind_time;

	/**
	 *
	 * @var integer
	 */
	public $order_score;

	/**
	 *
	 * @var integer
	 */
	public $order_send_score;

	/**
	 *
	 * @var integer
	 */
	public $pay_time;

	/**
	 *
	 * @var string
	 */
	public $barcode;

	/**
	 *
	 * @var integer
	 */
	public $unseemly_time;

	/**
	 *
	 * @var integer
	 */
	public $sort;

	/**
	 *
	 * @var integer
	 */
	public $competition_time;

	/**
	 *
	 * @var integer
	 */
	public $shops_state;

	/**
	 *
	 * @var integer
	 */
	public $refund_state;

	/**
	 *
	 * @var integer
	 */
	public $share_state;

	/**
	 *
	 * @var integer
	 */
	public $created_at;

	/**
	 *
	 * @var integer
	 */
	public $updated_at;

	/**
	 *
	 * @var integer
	 */
	public $deleted_at;

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'orders';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Orders[]
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Orders
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

}
